import time
import datetime
import pdb
import numpy as np
from IPython import embed
from ECModel.ecmodel import *
from scipy.interpolate import interp1d
import os
import scipy
import sys

# Simulation Parameters:
    # d_rate = dose rate in coulombs / minute
    # t_dose = dosing time in minutes
    # t_mix = mixing time in minutes
    # As_thresh = target dissolved arsenic concentration in moles / liter
    # Fe_thresh = target dissolved iron concentration in moles / liter
    # pH = steady-state pH (unitless)
    # Si_init = initial silicon concentration in moles / liter
    # As3_init = initial arsenic(III) concentration in moles / liter
    # As5_init = initial arsenic(V) concentration in moles / liter
    # P_init = initial phosphorus concentration in moles / liter
    # O2_init = initial O2 concentration in moles / liter
    # O2_sat = O2 concentration at saturation in moles / liter
    # k_app = rate constant for Fe2 oxidation
    # k_r = rate constant for O2 recharge in hours^-1 (also takes a value of "sat" to indicate saturated condition)
    # pH_deps = list of pH-dependent parameters, can include 'k' (adsorption), and 'g' (gamma)
    # alpha = total number of available sites / [Fe2(II)]ox
    # beta = coefficient (unitless)
    # gamma = k1/k2, with k1 = rate of Fe(II) oxidation, k2 = rate of As(III) oxidation
    # k_As3 = adsorption constant for As3 in liters / mole
    # k_As5 = adsorption constant for As5 in liters / mole
    # k_S = adsorption constant for Si in liters / mole
    # k_P = adsorption constant for P in liters / mole
    
params = {"d_rate": None,
          "t_dose": None,
          "t_mix": None,
          "As_thresh": None,
          "Fe_thresh": None,
          "pH": 7.1,
          "Si_init": 1.068E-003,
          "As3_init": 6.67E-006,
          "As5_init": 0.0,
          "P_init": 9.687E-005,
          "O2_init": 2.5E-4,
          "O2_sat": 2.5E-4,
          "k_app": 2.05045,
          "k_r": "sat",
          "pH_deps": [],
          "alpha": 0.81,
          "beta": 0.25,
          "gamma": 1.07,
          "k_As3": 10**3.9,
          "k_As5": 10**5.76,
          "k_S": 10**2.9,
          "k_P": 10**6.06} 

# Current User:

user = 'naddy'

# Executed Test:

def main():
    FeVTime()

# Test Functions:

# Gets As as a function of pH:
def AsVpH():
    dosage = 132
    params['d_rate'] = 3.0
    params['t_dose'] = dosage / params['d_rate']    
    t_mixes = [600, 200, 200, 200, 200, 200, 200]
    pHs = [6.6, 7.1, 7.2, 7.5, 7.8, 8.1, 8.6]
    pH_deps = [[], ['k'], ['k','g']]
    specs = ['Dosage = 132 C/L', 'Mixing time = 600 min for pH = 6.6, 200 min for all others', 'd_rate']
    matrix = Matrix(specs)
    for pH_dep in pH_deps:
        params['pH_deps'] = pH_dep
        matrix.condit(pH_dep, 'pH dependent parameters')
        for (i, pH) in enumerate(pHs):
            params['pH'] = pH
            params['t_mix'] = t_mixes[i]
            output = ECModel(params)
            As3_dis = molAsppb(output['As3_dis_final'])
            As5_dis = molAsppb(output['As5_dis_final'])
            As3_ads = molAsppb(output['As3_ads_final'])
            As5_ads = molAsppb(output['As5_ads_final'])
            matrix.enter(pH, 'pH')
            matrix.enter(As3_dis, 'As3_dis (ppb)')
            matrix.enter(As5_dis, 'As5_dis (ppb)')
            matrix.enter(As3_ads, 'As3_ads (ppb)')
            matrix.enter(As5_ads, 'As5_ads (ppb)')
    matrix.printFile('as_vs_pH.txt')

# Gets required iron as a function of dose rate
def IronVDoseRate(): 
    d_rates = np.linspace(1, 24, 5)
    params['As_thresh'] = ppbAsmol(10)
    params['t_mix'] = 120
    t_dose = 1
    specs = ['t_mix']
    matrix = Matrix(specs)
    for d_rate in d_rates:
        params['d_rate'] = d_rate
        t_dose = DoseTimeForThreshold(As_thresh, params, t_dose)
        iron = molFemg(t_dose * d_rate * 5.1822E-6) 
        matrix.enter(d_rate, 'Dosage Rate (C/L/m)')
        matrix.enter(iron, 'Iron (mg/L)')
    matrix.printFile('iron_vs_dose_rate.txt')

# Gets Fe2_dis and O2 saturation as a function of time
def FeVTime():
    params['d_rate'] = 10 # establishes the dose rate
    params['t_dose'] = 20 # establishes the dose time
    params['t_mix'] = 200 # establishes the mixing time
    k_rs = ['sat', 1.0] # a list of k_rs that will be iterated through during this test
    specs = ['d_rate', 't_dose', 't_mix'] # A list of constant parameters that should
                                          # be noted in the header of the output file.
                                          # Keys to the 'params' dictionary can be used here,
                                          # but if you want to handwrite your own condition,
                                          # you can also include it here.
    matrix = Matrix(specs) # initializes the matrix
    for k_r in k_rs:  # iterates through the k_rs
        params['k_r'] = k_r # establishes k_r under the current iteration
        matrix.condit(k_r, 'k_r') # establishes k_r as a test condition
        output = ECModel(params) # runs the simulation using current parameters
        for [i, t] in enumerate(output['t']): # iterates through each index in
                                              # the output time series
            t_min = t / 60 # converts time from seconds to minutes
            Fe2_dis = molFeppm(output['Fe2_dis'][i]) # gets Fe2_dis at the current
                                                     # point in time and converts its
                                                     # units to ppm
            O2 = (output['O2'][i] / params['O2_sat']) * 100 # gets O2 at the current
                                                            # point in time and converts
                                                            # its units to saturation %
            matrix.enter(t_min, 'Time (min') # enters current t_min as a datapoint
            matrix.enter(Fe2_dis, 'Fe2_dis (ppm)') # enters current Fe2_dis as
                                                   # a datapoint
            matrix.enter(O2, 'O2 (%)') # enters current O2 as a datapoint
    matrix.printFile('fe_vs_time.txt') # prints output to file with specified name

# Gets time required to achieve an Fe2_dis threshold as a function of dosage with fixed dose rate and varying k_r
def FeDepleteTimeVDosage():
    params['d_rate'] = 10
    params['Fe_thresh'] = 1e-7
    k_rs = np.linspace(2, 5, 5)
    dosages = np.logspace(1, 2, 4)
    t_mix = 120
    specs = ['d_rate', 'Fe_thresh']
    matrix = Matrix(specs)
    for k_r in k_rs:
        matrix.condit(k_r, 'k_r (hrs^-1)')
        params['k_r'] = k_r
        for dosage in dosages:
            params['t_dose'] = dosage / 10
            t_mix = MixTimeForThreshold(params, t_mix)
            t_tot = params['t_dose'] + t_mix
            matrix.enter(dosage, 'Dosage (C/L)')
            matrix.enter(t_tot, 'Time (min)')
    matrix.printFile('fe_deplete_time_vs_dosage.txt')

# Gets time required to achieve an Fe2_dis threshold as a function of dosage with fixed k_r and varying dose rate
def FeDepleteTimeVDosageVRate():
    params['k_r'] = 4.6
    params['Fe_thresh'] = 1e-7
    d_rates = np.linspace(3, 27, 5)
    dosages = np.logspace(1, 2, 4)
    t_mix = 120
    specs = ['k_r', 'Fe_thresh']
    matrix = Matrix(specs)
    for d_rate in d_rates:
        matrix.condit(d_rate, 'Dosage Rate (C/L/m)')
        params['d_rate'] = d_rate
        for dosage in dosages:
            params['t_dose'] = dosage / d_rate
            t_mix = MixTimeForThreshold(params, t_mix)
            t_tot = params['t_dose'] + t_mix
            matrix.enter(dosage, 'Dosage (C/L)')
            matrix.enter(t_tot, 'Time (min)')
    matrix.printFile('fe_deplete_time_vs_dosage_vs_rate.txt')

# Gets time required to achieve an As_dis threshold as a function of dosage with fixed dose rate and varying k_r
def AsDepleteTimeVDosage():
    params['d_rate'] = 10
    params['As_thresh'] = ppbAsmol(10)
    k_rs = np.linspace(2, 5, 5)
    dosages = np.linspace(400, 500, 10)
    t_mix = 120
    specs = ['d_rate', 'Fe_thresh']
    matrix = Matrix(specs)
    for k_r in k_rs:
        matrix.condit(k_r, 'k_r (hrs^-1)')
        params['k_r'] = k_r
        for dosage in dosages:
            params['t_dose'] = dosage / 10
            t_mix = MixTimeForThreshold(params, t_mix)
            t_tot = params['t_dose'] + t_mix
            matrix.enter(dosage, 'Dosage (C/L)')
            matrix.enter(t_tot, 'Time (min)')
    matrix.printFile('as_deplete_time_vs_dosage.txt')

# Gets time required to achieve an As_dis threshold as a function of dosage with fixed k_r and varying dose rate
def AsDepleteTimeVDosageVRate():
    params['k_r'] = 4.6
    params['As_thresh'] = ppbAsmol(10)
    d_rates = np.linspace(3, 27, 5)
    dosages = np.linspace(400, 500, 10)
    t_mix = 120
    specs = ['k_r', 'As_thresh']
    matrix = Matrix(specs)
    for d_rate in d_rates:
        matrix.condit(d_rate, 'Dosage (C/L/m)')
        params['d_rate'] = d_rate
        for dosage in dosages:
            params['t_dose'] = dosage / d_rate
            t_mix = MixTimeForThreshold(params, t_mix)
            t_tot = params['t_dose'] + t_mix
            matrix.enter(dosage, 'Dosage (C/L)')
            matrix.enter(t_tot, 'Time (min)')
    PrintToFile('as_deplete_time_vs_dosage_vs_rate.txt', condits, labels, matrix)

# Gets all dis/ads As species as a function of dosage with fixed dose rate
def AsVDosage():
    params['t_mix'] = 120
    params['d_rate'] = 3.1
    dosages = [25, 50, 100, 250]
    specs = ['t_mix', 'd_rate']
    matrix = Matrix(specs)
    for dosage in dosages:
        params['t_dose'] = dosage / 3.1
        output = ECModel(params)
        As3_dis = output['As3_dis_final']
        As3_ads = output['As3_ads_final']
        As5_dis = output['As5_dis_final']
        As5_ads = output['As5_ads_final']
        matrix.enter(dosage, 'Dosage (C/L)')
        matrix.enter(As3_dis, 'As3_dis (M)')
        matrix.enter(As3_ads, 'As3_ads (M)')
        matrix.enter(As5_dis, 'As5_dis (M)')
        matrix.enter(As5_ads, 'As5_ads (M)')        
    matrix.printFile('as_vs_dosage.txt')

# Test Matrix Class: 

class Matrix:
    def __init__(self, specs): # initializes matrix from specs
        self.specs = specs
        self.data = []
        self.layers = 1

    def condit(self, value, name): # creates a test condition under which data can be entered
        str_value = str(value).replace("'","")
        label = name + ' = ' + str_value
        self.data.append([label, []])
        if self.layers == 1:
            self.layers = 2

    def enter(self, value, name): # enters data, either directly into the matrix, or under a test condition
        if self.layers == 1:
            label_exists = False
            for col in self.data:
                if col[0] == name:
                    label_exists = True
                    col.append(value)
            if label_exists == False:
                self.data.append([name, value])
        else:
            label_exists = False
            for col in self.data[-1][1]:
                if col[0] == name:
                    label_exists = True
                    col.append(value)
            if label_exists == False:
                self.data[-1][1].append([name, value])

    def printFile(self, filename): # prints the entire matrix to a text file with a user-specified filename,
                                   # under the user's directory
        header_row = str(datetime.datetime.now()) + ',' + user + '\n'
        specs_row = ''
        for spec in self.specs:
            if spec == 'd_rate':
                units = 'C/L/m'
            elif spec in ('t_dose', 't_mix'):
                units = 'min'
            elif spec in ('As_thresh', 'Fe_thresh', 'Si_init', 'As3_init', 'As5_init', 'P_init', 'O2_init', 'O2_sat'):
                units = 'mols / L'
            elif spec == 'k_r':
                units = 'hrs^-1'
            elif spec in ('k_As3', 'k_As5', 'k_S', 'k_P'):
                units = 'L / mol'
            else:
                units = ''
            if spec in params.keys():
                spec_val = params[spec]
                specs_row = specs_row + spec + ' = ' + str(spec_val) + ' ' + units + ','
            else:
                specs_row = specs_row + spec + ','
        specs_row = specs_row + '\n'

        data_rows = ''
        if self.layers == 1:
            for i in range(len(self.data[0])):
                for col in self.data:
                    data_rows = data_rows + str(col[i]) + ','
                data_rows = data_rows + '\n'

        else:
            n_cols = len(self.data[0][1])
            for data_set in self.data:
                data_rows = data_rows + data_set[0] + ','
                for i in range(n_cols):
                    data_rows = data_rows + ','
            data_rows = data_rows + '\n'
            for i in range(len(self.data[0][1][0])):
                for data_set in self.data:
                    for j in range(n_cols):
                        data_rows = data_rows + str(data_set[1][j][i]) + ','
                    data_rows = data_rows + ','
                data_rows = data_rows + '\n'

        rel_path = 'users/' + user + '/' + filename
        script_dir = os.path.dirname(__file__)
        abs_path = os.path.join(script_dir, rel_path)
        with open(abs_path, 'w') as f:
            f.write(header_row + specs_row + data_rows)

# Common Functions:

def Root(func, x_init, xtol = 0.1, ytol = 1e-10): # used to solve iteration-based problems
    x0 = x_init
    y0 = func(x0)
    z = x0
    while abs(y0) > ytol and abs(z) > xtol:
        x1 = x0 - (y0 / ((func(x0 + 0.2) - y0) / 0.2))
        if x1 < 0:
            print 'lowbound error'
            x1 = 10
        z = x1 - x0
        x0 = x1
        y0 = func(x0)
    return x0

def MixTimeForThreshold(params, t_init): # iteratively solves for mixing time to achieve an Fe2_dis or As_dis threshold
    def Residual(t_mix):
        params['t_mix'] = t_mix
        output = ECModel(params)
        if params['Fe_thresh'] <> None:
            residual = output['Fe2_dis_final'] - params['Fe_thresh']
            print [t_mix, residual]
            return residual
        elif params['As_thresh'] <> None:
            residual = output['As3_dis_final'] + output['As5_dis_final'] - params['As_thresh']
            print [t_mix, residual]
            return residual
    t_mix = Root(Residual, t_init)
    return t_mix

def DoseTimeForThreshold(params, t_init): # iteratively solves for dose time to achieve an As_dis threshold
    def Residual(t_dose):
        params['t_dose'] = t_dose
        output = ECModel(params)
        residual = output['As3_dis_final'] + output['As5_dis_final'] - params['As_thresh']
        print [t_dose, residual]
        return residual
    t_dose = Root(Residual, t_init)
    return t_dose

def DoseRateForThreshold(params, d_init): # iteratively solves for dose rate to achieve an As_dis threshold
    def Residual(d_rate):
        params['d_rate'] = d_rate
        output = ECModel(params)
        residual = output['As3_dis_final'] + output['As5_dis_final'] - params['As_thresh']
        print [d_rate, residual]
        return residual
    d_rate = Root(Residual, d_init)
    return d_rate

# Conversion Functions:

def molAsppb(As): # converts moles / liter As --> ppb
    return As / 1.335E-8

def ppbAsmol(As): # converts ppb As --> moles / liter
    return As * 1.335E-8

def molFeppm(Fe): # converts moles / liter Fe --> ppm
    return Fe / 1.791E-5

def ppmFemol(Fe): # converts ppm Fe --> moles / liter
    return Fe * 1.791E-5

def molFemg(Fe): # converts moles / liter Fe to mg / liter
    return Fe * 55845

def mgFemol(Fe): # converts mg / liter Fe to moles / liter
    return Fe / 55845

if __name__ == '__main__':
    embed()
    main()

import time
import datetime
import pdb
import copy
import uuid
import numpy as np
from ECModel.ecmodel import *
from scipy.interpolate import interp1d
import ecox.ecmodel.ecmodel as ecmodel
from itertools import product
import os
import scipy
import sys
import yaml
from IPython import embed
from optparse import OptionParser # For backwards compatibility with python 2.5, 2.6
import logging

logger = logging.getLogger('ecox_sim')
LOG_FORMAT = '%(name)s - %(asctime)s - %(levelname)s - %(message)s'
logger.setLevel(logging.INFO)

ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter(LOG_FORMAT))

logger.addHandler(ch)

final_vars = ['As5_ads_final','P_dis_final','Si_dis_final',
              'As3_dis_final','P_ads_final','Fe2_dis_final',
              'As3_ads_final', 'Si_ads_final','As5_dis_final','O2_final']

dynamic_vars = ['Fe2_ox','Fe2_dis','As3_tot','As3_ox','As5_tot','O2']



# Simulation Parameters:
    # d_rate = dose rate in coulombs / minute
    # t_dose = dosing time in minutes
    # t_mix = mixing time in minutes
    # As_thresh = target dissolved arsenic concentration in moles / liter
    # Fe_thresh = target dissolved iron concentration in moles / liter
    # pH = steady-state pH (unitless)
    # Si_init = initial silicon concentration in moles / liter
    # As3_init = initial arsenic(III) concentration in moles / liter
    # As5_init = initial arsenic(V) concentration in moles / liter
    # P_init = initial phosphorus concentration in moles / liter
    # O2_init = initial O2 concentration in moles / liter
    # O2_sat = O2 concentration at saturation in moles / liter
    # k_app = rate constant for Fe2 oxidation
    # k_r = rate constant for O2 recharge in hours^-1 (also takes a value of "sat" to indicate saturated condition)
    # pH_deps = list of pH-dependent parameters, can include 'k' (adsorption), and 'g' (gamma)
    # alpha = total number of available sites / [Fe2(II)]ox
    # beta = coefficient (unitless)
    # gamma = k1/k2, with k1 = rate of Fe(II) oxidation, k2 = rate of As(III) oxidation
    # k_As3 = adsorption constant for As3 in liters / mole
    # k_As5 = adsorption constant for As5 in liters / mole
    # k_S = adsorption constant for Si in liters / mole
    # k_P = adsorption constant for P in liters / mole

def main():
    FeVTime()

class RunMode(object):
    '''Represents '''

    class _RunMode(object):
        def __init__(self, val):
            self.val = val

        def __lt__(self, mode):
            return self.val % mode.val == 0

        def __gt__(self, mode):
            return mode.val % self.val == 0
    
    single   = _RunMode(2)
    multiple = _RunMode(3)
    standard = _RunMode(4)
    solve = _RunMode(8)



def generateParameztrizedRunName(yaml_input):
    date_str = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    id_str = yaml_input['system-info']['id'].split("-")[0]
    return "ParameterizedRun_{0}_{1}".format(date_str, id_str)    


def generateRunName(yaml_input):
    date_str = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    id_str = yaml_input['system-info']['id'].split("-")[0]
    return "Run_{0}_{1}".format(date_str, id_str)

def generateRunCode():
    return str(uuid.uuid4())

def main(input_fn, output_dir):
    # We can run either a single mode or a paramatrized mode.
    run_code = generateRunCode()
    try:
        inp = loadAndValidateYaml(input_fn)
    except InputException, xcpt:
        logger.error("Yaml Input File Error: {0}".format(xcpt.message))
        logger.critical("Quitting.")
        sys.exit(1)

    run_name = generateRunName(inp)

    try:
        ph_deps = map(lambda x: x.strip(), inp['model-params']['pH_deps'].strip().split(","))
    except:
        ph_deps = []

    kr = parseParamStringIntoRange(inp['params']['k_r'])
    if not kr: kr = inp['params']['k_r'].strip().split()
                                   
    
    params = {'d_rate': parseParamStringIntoRange(inp['params']['dose_rate']),
              't_dose': parseRunParamsString(inp['run-params']['dose_time']),
              't_mix': parseRunParamsString(inp['run-params']['mix_time']),
              'pH': parseParamStringIntoRange(inp['params']['pH']),
              'Si_init': parseParamStringIntoRange(inp['params']['Si_init']),
              'As3_init': parseParamStringIntoRange(inp['params']['As3_init']),
              'As5_init': parseParamStringIntoRange(inp['params']['As5_init']),
              'P_init': parseParamStringIntoRange(inp['params']['P_init']),
              'O2_init': parseParamStringIntoRange(inp['params']['O2_init']),
              'O2_sat': parseParamStringIntoRange(inp['params']['O2_sat']),
              'k_app': parseParamStringIntoRange(inp['params']['k_app']),
              'alpha': parseParamStringIntoRange(inp['params']['alpha']),
              'beta': parseParamStringIntoRange(inp['params']['beta']),
              'gamma': parseParamStringIntoRange(inp['params']['gamma']),
              'k_As3': parseParamStringIntoRange(inp['params']['k_As3']),
              'k_As5': parseParamStringIntoRange(inp['params']['k_As5']),
              'k_S': parseParamStringIntoRange(inp['params']['k_S']),
              'k_P': parseParamStringIntoRange(inp['params']['k_P']),
              'k_r': kr,
              'pH_deps': ph_deps,
              }

    parametrized_keys = ['k_As3','k_As5','O2_sat','k_P','k_S','k_r','beta',
                         'As5_init','As3_init','t_mix','O2_init','gamma','k_app',
                         'alpha','pH','P_init','Si_init','d_rate', 't_dose']

    if max([len(params[x]) for x in parametrized_keys]) == 1:
        outname_infile = os.path.join(output_dir, "{0}.yin".format(run_name))
        outname_outfile = os.path.join(output_dir, "{0}.csv".format(run_name))

        logger.info("Writing output file {0}".format(outname_infile))
        outfile = open(outname_infile, 'w')
        outfile.write(yaml.dump(inp, default_flow_style=False))

        logger.info("Running single run with parameters: {0}".format(params))
        
        params = copy.deepcopy(params)
        for k in parametrized_keys:
            params[k] = params[k][0]
        single_run_results = runSingleRunWithParams(params)

        logger.info("Writing output to {0}".format(outname_outfile))
        writeSingleRunResults(outname_outfile, inp, single_run_results)
    else:
        logger.info("Running Paramatrized run.")
        parametarized_run_name = generateParameztrizedRunName(inp)

        output_dir = os.path.join(output_dir, parametarized_run_name)
        os.mkdir(output_dir)

        values = [params[x][:] for x in parametrized_keys]
        iterations = [x for x in product(*values)]

        all_output = {}

        for ndx, it in enumerate(iterations):
            logger.info("Running parameterized Run {0}/{1}".format(ndx+1, len(iterations)))
            local_params = copy.deepcopy(params)
            local_yaml = copy.deepcopy(inp)

            local_yaml['system-info']['parent-id'] = local_yaml['system-info']['id']
            local_yaml['system-info']['id'] = generateRunCode()
            
            for (param, value) in zip(parametrized_keys, it):
                local_params[param] = [value]
                local_yaml['params'][param] = value

            local_params = copy.deepcopy(local_params)
            for k in parametrized_keys:
                local_params[k] = local_params[k][0]

            param_run_name = generateRunName(local_yaml)
            outname_infile = os.path.join(output_dir, "{0}.yin".format(param_run_name))
            outname_outfile = os.path.join(output_dir, "{0}.csv".format(param_run_name))
            run_results = runSingleRunWithParams(local_params)

            outfile = open(outname_infile, 'w')
            outfile.write(yaml.dump(local_yaml, default_flow_style=False))

            logger.info("Writing output to {0}".format(outname_outfile))
            writeSingleRunResults(outname_outfile, local_yaml, run_results)
            all_output[it] = copy.deepcopy(run_results)

        # Write the paramatrization info
        outname_infile = os.path.join(output_dir, "{0}.yin".format(parametarized_run_name))
        outfile = open(outname_infile, 'w')
        outfile.write(yaml.dump(inp, default_flow_style=False))

        # Write the file that lists each of the parameterizations
        outname_parameterlist_file = os.path.join(output_dir, "{0}.paramruns".format(parametarized_run_name))
        outfile = open(outname_parameterlist_file, 'w')
        for (ndx, it) in enumerate(iterations):
            outfile.write("{0}: {1}\n".format(ndx, dict(zip(parametrized_keys, it))))
        outfile.close()

        outname_finalval_file = os.path.join(output_dir, "{0}.final".format(parametarized_run_name))
        outfile =  open(outname_finalval_file, 'w')
        outfile.write("variable,")
        outfile.write(",".join(map(str, range(len(iterations)))))
        outfile.write("\n")

        for variable in final_vars:
            outfile.write(variable)
            outfile.write(',')

            val_array = [all_output[it][variable] for it in iterations]
            my_str = ",".join(map(str, val_array))
            outfile.write("{0}\n".format(my_str))

        for variable in dynamic_vars:
            var_outfn = os.path.join(output_dir, "{0}.{1}.csv".format(parametarized_run_name, variable.lower()))
            outfile = open(var_outfn, 'w')

            outfile.write("t,")
            outfile.write(",".join(map(str, range(len(iterations)))))
            outfile.write("\n")


            for ndx in range(len(all_output.values()[0]['t'])):
                outfile.write(str(all_output.values()[0]['t'][ndx]))
                outfile.write(",")
                val_vals = [all_output[it][variable][ndx] for it in iterations]
                outfile.write(",".join(map(str, val_vals)))
                outfile.write("\n")
        outfile.close()            
    return

def writeSingleRunResults(output_fn, inp_yaml, single_run_results):
    outfile = open(output_fn, 'w')

    ## WRITE HEADER HERE
    outfile.write("id: {0}\n".format(inp_yaml['system-info']['id']))
    outfile.write("date-created: {0}\n".format(inp_yaml['system-info']['date-created']))
    outfile.write("---\n")

    for key in final_vars:
        outfile.write("{0}: {1}\n".format(key, single_run_results[key]))
    outfile.write("---\n")
                    

    tt = single_run_results['t']

    outfile.write('t,')
    outfile.write(",".join(dynamic_vars))
    outfile.write("\n")

    for ndx in xrange(len(tt)):
        outfile.write("{0},".format(tt[ndx]))

        row_vals = map(lambda col: single_run_results[col][ndx], dynamic_vars)
        row_str = ",".join(map(str, row_vals))

        outfile.write(row_str)
        outfile.write("\n")

 
def runSingleRunWithParams(params):
    return ecmodel.simulateECModel(params)

def paramsAreSingleRun(params):
    return max(map(len, params.values())) == 1

def parseFloat(string):
    if "**" in string:
        comp = string.split("**")
        if not len(comp) == 2: raise ValueError("Expected two components.")
        return float(comp[0]) ** float(comp[1])
    else:
        return float(string)

def parseParamStringIntoRange(param_str):
    param_str = getYamlValue(param_str)
    try:
        components = param_str.strip().split(",")
        param_range = [parseFloat(x) for x in components]
    except ValueError:
        return False
    return param_range

def getBestSecondsFromString(str):
    components = str.strip().split()
    components = [x.strip() for x in components if x.strip()]
    return sum(map(_getBestSec, components))

def _getBestSec(conf_string):
    
    """
    Takes a string like 20, 20.0, 21.3s 29m, 30.1h and returns a
    floating number of seconds.
    
    Keyword arguments:
    conf (str): A string representing a number using the [A<.B>[ shmd] syntax.
    
    Returns:
    float: the number of seconds represented by the string
    """

    conf_string=conf_string.strip()

    S,M,H,D,T=1,2,3,4,5
    interval = S

    if conf_string[-1].lower() not in "smdhy": return float(conf_string)

    float_str, val = conf_string[:-1], conf_string[-1].lower()

    if val == "s":
        return float(float_str)
    elif val == "m":
        return float(float_str) * 60
    elif val == "h":
        return float(float_str) * 3600
    elif val == "d":
        return float(float_str) * 3600 * 24
    elif val == "y":
        return float(float_str) * 3600 * 24 * 365
    else:
        raise Exception("Unknown error")

    return

def getYamlValue(value):
    value = str(value)
    is_comment = value.find("#")
    if is_comment != -1:
        value = value[:is_comment]
    return value.strip()

def parseRunParamsString(run_param_str):
    run_param_str = getYamlValue(run_param_str)
    try:
        return [getBestSecondsFromString(run_param_str)]
    except ValueError:
        return False

def checkStringIsValidRunParamsValue(val_str):
    return parseRunParamsString(val_str)

def checkStringIsValidParamsValue(val_str):
    return parseParamStringIntoRange(val_str)

def loadAndValidateYaml(input_fn):
    def re(msg): raise InputException(msg)

    inp = yaml.load(open(input_fn))
    if not 'info' in inp: re("Input file must have an 'info' section.")
    if not 'run-params' in inp: re("Input file must have an 'run-params' section.")
    if not 'params' in inp: re("Input file must have a 'params' section.")

    runparams_keys = ['dose_time', 'mix_time']
    param_keys = ['As3_init','As5_init','O2_init','O2_sat','P_init','Si_init','alpha','beta','gamma','k_As3','k_As5','k_P','k_S','k_app', 'pH', 'dose_rate']

    for k in runparams_keys:
        if not k in inp['run-params']: re("Expected '{0}' parameter in 'run-params' section of file.".format(k))
        val = inp['run-params'][k]
        if not checkStringIsValidRunParamsValue(val):
            re("Value '{0}' of parameter {1} in section 'run-params' is not valid".format(val, k))

    for k in param_keys:
        if not k in inp['params']: re("Expected '{0}' parameter in 'run-params' section of file.".format(k))
        val = inp['params'][k]
        if not checkStringIsValidParamsValue(val):
            re("Value '{0}' of parameter {1} in section 'params' is not valid".format(val, k))

    if not 'k_r' in inp['params']: re("Expected '{0}' parameter in 'run-params' section of file.".format('k_r'))
    if not checkStringIsValidParamsValue(inp['params']['k_r']) and inp['params']['k_r'] not in ['sat']:
        re("Expected k_r to be a float or 'sat'")

    # ,'pH_deps'

    ##################################
    # Update the system-info section
    #
    ##################################
    
    try:
        parent_id = inp['system-info']['id']
    except KeyError:
        parent_id = "None"

    inp['system-info'] = {}
    inp['system-info']['date-created'] = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
    inp['system-info']['parent-file'] = input_fn

    inp['system-info']['parent-id'] = parent_id
    inp['system-info']['id'] = generateRunCode()
    return inp

# Test Functions:
# Gets As as a function of pH:
def AsVpH():
    dosage = 132
    params['d_rate'] = 3.0
    params['t_dose'] = dosage / params['d_rate']    
    t_mixes = [600, 200, 200, 200, 200, 200, 200]
    pHs = [6.6, 7.1, 7.2, 7.5, 7.8, 8.1, 8.6]
    pH_deps = [[], ['k'], ['k','g']]
    specs = ['Dosage = 132 C/L', 'Mixing time = 600 min for pH = 6.6, 200 min for all others', 'd_rate']
    matrix = Matrix(specs)
    for pH_dep in pH_deps:
        params['pH_deps'] = pH_dep
        matrix.condit(pH_dep, 'pH dependent parameters')
        for (i, pH) in enumerate(pHs):
            params['pH'] = pH
            params['t_mix'] = t_mixes[i]
            output = ECModel(params)
            As3_dis = molAsppb(output['As3_dis_final'])
            As5_dis = molAsppb(output['As5_dis_final'])
            As3_ads = molAsppb(output['As3_ads_final'])
            As5_ads = molAsppb(output['As5_ads_final'])
            matrix.enter(pH, 'pH')
            matrix.enter(As3_dis, 'As3_dis (ppb)')
            matrix.enter(As5_dis, 'As5_dis (ppb)')
            matrix.enter(As3_ads, 'As3_ads (ppb)')
            matrix.enter(As5_ads, 'As5_ads (ppb)')
    matrix.printFile('as_vs_pH.txt')

# Gets required iron as a function of dose rate
def IronVDoseRate(): 
    d_rates = np.linspace(1, 24, 5)
    params['As_thresh'] = ppbAsmol(10)
    params['t_mix'] = 120
    t_dose = 1
    specs = ['t_mix']
    matrix = Matrix(specs)
    for d_rate in d_rates:
        params['d_rate'] = d_rate
        t_dose = DoseTimeForThreshold(As_thresh, params, t_dose)
        iron = molFemg(t_dose * d_rate * 5.1822E-6) 
        matrix.enter(d_rate, 'Dosage Rate (C/L/m)')
        matrix.enter(iron, 'Iron (mg/L)')
    matrix.printFile('iron_vs_dose_rate.txt')

# Gets Fe2_dis and O2 saturation as a function of time
def FeVTime():
    params['d_rate'] = 10 # establishes the dose rate
    params['t_dose'] = 20 # establishes the dose time
    params['t_mix'] = 200 # establishes the mixing time
    k_rs = ['sat', 1.0] # a list of k_rs that will be iterated through during this test
    specs = ['d_rate', 't_dose', 't_mix'] # A list of constant parameters that should
                                          # be noted in the header of the output file.
                                          # Keys to the 'params' dictionary can be used here,
                                          # but if you want to handwrite your own condition,
                                          # you can also include it here.
    matrix = Matrix(specs) # initializes the matrix
    for k_r in k_rs:  # iterates through the k_rs
        params['k_r'] = k_r # establishes k_r under the current iteration
        matrix.condit(k_r, 'k_r') # establishes k_r as a test condition
        output = ECModel(params) # runs the simulation using current parameters
        for [i, t] in enumerate(output['t']): # iterates through each index in
                                              # the output time series
            t_min = t / 60 # converts time from seconds to minutes
            Fe2_dis = molFeppm(output['Fe2_dis'][i]) # gets Fe2_dis at the current
                                                     # point in time and converts its
                                                     # units to ppm
            O2 = (output['O2'][i] / params['O2_sat']) * 100 # gets O2 at the current
                                                            # point in time and converts
                                                            # its units to saturation %
            matrix.enter(t_min, 'Time (min') # enters current t_min as a datapoint
            matrix.enter(Fe2_dis, 'Fe2_dis (ppm)') # enters current Fe2_dis as
                                                   # a datapoint
            matrix.enter(O2, 'O2 (%)') # enters current O2 as a datapoint
    matrix.printFile('fe_vs_time111.txt') # prints output to file with specified name

# Gets time required to achieve an Fe2_dis threshold as a function of dosage with fixed dose rate and varying k_r
def FeDepleteTimeVDosage():
    params['d_rate'] = 10
    params['Fe_thresh'] = 1e-7
    k_rs = np.linspace(2, 5, 5)
    dosages = np.logspace(1, 2, 4)
    t_mix = 120
    specs = ['d_rate', 'Fe_thresh']
    matrix = Matrix(specs)
    for k_r in k_rs:
        matrix.condit(k_r, 'k_r (hrs^-1)')
        params['k_r'] = k_r
        for dosage in dosages:
            params['t_dose'] = dosage / 10
            t_mix = MixTimeForThreshold(params, t_mix)
            t_tot = params['t_dose'] + t_mix
            matrix.enter(dosage, 'Dosage (C/L)')
            matrix.enter(t_tot, 'Time (min)')
    matrix.printFile('fe_deplete_time_vs_dosage.txt')

# Gets time required to achieve an Fe2_dis threshold as a function of dosage with fixed k_r and varying dose rate
def FeDepleteTimeVDosageVRate():
    params['k_r'] = 4.6
    params['Fe_thresh'] = 1e-7
    d_rates = np.linspace(3, 27, 5)
    dosages = np.logspace(1, 2, 4)
    t_mix = 120
    specs = ['k_r', 'Fe_thresh']
    matrix = Matrix(specs)
    for d_rate in d_rates:
        matrix.condit(d_rate, 'Dosage Rate (C/L/m)')
        params['d_rate'] = d_rate
        for dosage in dosages:
            params['t_dose'] = dosage / d_rate
            t_mix = MixTimeForThreshold(params, t_mix)
            t_tot = params['t_dose'] + t_mix
            matrix.enter(dosage, 'Dosage (C/L)')
            matrix.enter(t_tot, 'Time (min)')
    matrix.printFile('fe_deplete_time_vs_dosage_vs_rate.txt')

# Gets time required to achieve an As_dis threshold as a function of dosage with fixed dose rate and varying k_r
def AsDepleteTimeVDosage():
    params['d_rate'] = 10
    params['As_thresh'] = ppbAsmol(10)
    k_rs = np.linspace(2, 5, 5)
    dosages = np.linspace(400, 500, 10)
    t_mix = 120
    specs = ['d_rate', 'Fe_thresh']
    matrix = Matrix(specs)
    for k_r in k_rs:
        matrix.condit(k_r, 'k_r (hrs^-1)')
        params['k_r'] = k_r
        for dosage in dosages:
            params['t_dose'] = dosage / 10
            t_mix = MixTimeForThreshold(params, t_mix)
            t_tot = params['t_dose'] + t_mix
            matrix.enter(dosage, 'Dosage (C/L)')
            matrix.enter(t_tot, 'Time (min)')
    matrix.printFile('as_deplete_time_vs_dosage.txt')

# Gets time required to achieve an As_dis threshold as a function of dosage with fixed k_r and varying dose rate
def AsDepleteTimeVDosageVRate():
    params['k_r'] = 4.6
    params['As_thresh'] = ppbAsmol(10)
    d_rates = np.linspace(3, 27, 5)
    dosages = np.linspace(400, 500, 10)
    t_mix = 120
    specs = ['k_r', 'As_thresh']
    matrix = Matrix(specs)
    for d_rate in d_rates:
        matrix.condit(d_rate, 'Dosage (C/L/m)')
        params['d_rate'] = d_rate
        for dosage in dosages:
            params['t_dose'] = dosage / d_rate
            t_mix = MixTimeForThreshold(params, t_mix)
            t_tot = params['t_dose'] + t_mix
            matrix.enter(dosage, 'Dosage (C/L)')
            matrix.enter(t_tot, 'Time (min)')
    PrintToFile('as_deplete_time_vs_dosage_vs_rate.txt', condits, labels, matrix)

# Gets all dis/ads As species as a function of dosage with fixed dose rate
def AsVDosage():
    params['t_mix'] = 120
    params['d_rate'] = 3.1
    dosages = [25, 50, 100, 250]
    specs = ['t_mix', 'd_rate']
    matrix = Matrix(specs)
    for dosage in dosages:
        params['t_dose'] = dosage / 3.1
        output = ECModel(params)
        As3_dis = output['As3_dis_final']
        As3_ads = output['As3_ads_final']
        As5_dis = output['As5_dis_final']
        As5_ads = output['As5_ads_final']
        matrix.enter(dosage, 'Dosage (C/L)')
        matrix.enter(As3_dis, 'As3_dis (M)')
        matrix.enter(As3_ads, 'As3_ads (M)')
        matrix.enter(As5_dis, 'As5_dis (M)')
        matrix.enter(As5_ads, 'As5_ads (M)')        
    matrix.printFile('as_vs_dosage.txt')

# Test Matrix Class: 

class Matrix:
    def __init__(self, specs): # initializes matrix from specs
        self.specs = specs
        self.data = []
        self.layers = 1

    def condit(self, value, name): # creates a test condition under which data can be entered
        str_value = str(value).replace("'","")
        label = name + ' = ' + str_value
        self.data.append([label, []])
        if self.layers == 1:
            self.layers = 2

    def enter(self, value, name): # enters data, either directly into the matrix, or under a test condition
        if self.layers == 1:
            label_exists = False
            for col in self.data:
                if col[0] == name:
                    label_exists = True
                    col.append(value)
            if label_exists == False:
                self.data.append([name, value])
        else:
            label_exists = False
            for col in self.data[-1][1]:
                if col[0] == name:
                    label_exists = True
                    col.append(value)
            if label_exists == False:
                self.data[-1][1].append([name, value])

    def printFile(self, filename): # prints the entire matrix to a text file with a user-specified filename,
                                   # under the user's directory
        header_row = str(datetime.datetime.now()) + ',' + user + '\n'
        specs_row = ''
        for spec in self.specs:
            if spec == 'd_rate':
                units = 'C/L/m'
            elif spec in ('t_dose', 't_mix'):
                units = 'min'
            elif spec in ('As_thresh', 'Fe_thresh', 'Si_init', 'As3_init', 'As5_init', 'P_init', 'O2_init', 'O2_sat'):
                units = 'mols / L'
            elif spec == 'k_r':
                units = 'hrs^-1'
            elif spec in ('k_As3', 'k_As5', 'k_S', 'k_P'):
                units = 'L / mol'
            else:
                units = ''
            if spec in params.keys():
                spec_val = params[spec]
                specs_row = specs_row + spec + ' = ' + str(spec_val) + ' ' + units + ','
            else:
                specs_row = specs_row + spec + ','
        specs_row = specs_row + '\n'

        data_rows = ''
        if self.layers == 1:
            for i in range(len(self.data[0])):
                for col in self.data:
                    data_rows = data_rows + str(col[i]) + ','
                data_rows = data_rows + '\n'

        else:
            n_cols = len(self.data[0][1])
            for data_set in self.data:
                data_rows = data_rows + data_set[0] + ','
                for i in range(n_cols):
                    data_rows = data_rows + ','
            data_rows = data_rows + '\n'
            for i in range(len(self.data[0][1][0])):
                for data_set in self.data:
                    for j in range(n_cols):
                        data_rows = data_rows + str(data_set[1][j][i]) + ','
                    data_rows = data_rows + ','
                data_rows = data_rows + '\n'

        rel_path = 'users/' + user + '/' + filename
        script_dir = os.path.dirname(__file__)
        abs_path = os.path.join(script_dir, rel_path)
        with open(abs_path, 'w') as f:
            f.write(header_row + specs_row + data_rows)

# Common Functions:

def Root(func, x_init, xtol = 0.1, ytol = 1e-10): # used to solve iteration-based problems
    x0 = x_init
    y0 = func(x0)
    z = x0
    while abs(y0) > ytol and abs(z) > xtol:
        x1 = x0 - (y0 / ((func(x0 + 0.2) - y0) / 0.2))
        if x1 < 0:
            print 'lowbound error'
            x1 = 10
        z = x1 - x0
        x0 = x1
        y0 = func(x0)
    return x0

def MixTimeForThreshold(params, t_init): # iteratively solves for mixing time to achieve an Fe2_dis or As_dis threshold
    def Residual(t_mix):
        params['t_mix'] = t_mix
        output = ECModel(params)
        if params['Fe_thresh'] <> None:
            residual = output['Fe2_dis_final'] - params['Fe_thresh']
            print [t_mix, residual]
            return residual
        elif params['As_thresh'] <> None:
            residual = output['As3_dis_final'] + output['As5_dis_final'] - params['As_thresh']
            print [t_mix, residual]
            return residual
    t_mix = Root(Residual, t_init)
    return t_mix

def DoseTimeForThreshold(params, t_init): # iteratively solves for dose time to achieve an As_dis threshold
    def Residual(t_dose):
        params['t_dose'] = t_dose
        output = ECModel(params)
        residual = output['As3_dis_final'] + output['As5_dis_final'] - params['As_thresh']
        print [t_dose, residual]
        return residual
    t_dose = Root(Residual, t_init)
    return t_dose

def DoseRateForThreshold(params, d_init): # iteratively solves for dose rate to achieve an As_dis threshold
    def Residual(d_rate):
        params['d_rate'] = d_rate
        output = ECModel(params)
        residual = output['As3_dis_final'] + output['As5_dis_final'] - params['As_thresh']
        print [d_rate, residual]
        return residual
    d_rate = Root(Residual, d_init)
    return d_rate

# Conversion Functions:

def molAsppb(As): # converts moles / liter As --> ppb
    return As / 1.335E-8

def ppbAsmol(As): # converts ppb As --> moles / liter
    return As * 1.335E-8

def molFeppm(Fe): # converts moles / liter Fe --> ppm
    return Fe / 1.791E-5

def ppmFemol(Fe): # converts ppm Fe --> moles / liter
    return Fe * 1.791E-5

def molFemg(Fe): # converts moles / liter Fe to mg / liter
    return Fe * 55845

def mgFemol(Fe): # converts mg / liter Fe to moles / liter
    return Fe / 55845

class InputException(Exception): pass

def validateOptionsAndArguments(options, args):
    if not len(args) == 1: raise InputException("Must supply an input filename.")
    fn = args[0]
    if not os.path.isfile(fn): raise InputException("Input {0} is not a file.".format(fn))
    if not os.path.isdir(options.output_dir): raise InputException("Provided output directory '{0}' is not a directory".format(options.output_dir))
    return 

if __name__ == '__main__':

    parser = OptionParser()
    parser.add_option("--output-directory", dest="output_dir", default=".")
    parser.add_option("--quiet", dest="quiet", action="store_true", default=False)    
    parser.add_option("--verbose", dest="verbose", action="store_true", default=False)
    (options, args) = parser.parse_args()

    if options.quiet: logger.setLevel(logging.WARN)
    if options.verbose: logger.setLevel(logging.DEBUG)

    try:
        validateOptionsAndArguments(options, args)
    except InputException, xcpt:
        logger.error("Input Error: {0}".format(xcpt.message))
        logger.critical("Quitting.")
        sys.exit(1)

    input_fn = args[0]
    logger.info("Input filename: {0}".format(input_fn))
    logger.info("Output directory: {0}".format(options.output_dir))

    input_fn = os.path.abspath(input_fn)
    output_dir = os.path.abspath(options.output_dir)

    main(input_fn, output_dir)

    

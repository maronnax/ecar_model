env: env-dev githooks

requirements:
	@echo "Rebuilding the requirement files."
	pip freeze --local > requirements.txt


HOOK_FILES := $(shell ls githooks)
assert_uptodate_githooks:
	@# Passes if each of the githooks is mapped to .git/githooks."
	@for f in ${HOOK_FILES}; do cmp githooks/$$f .git/hooks/$$f || exit 1; done


githooks:
	@echo "Copying githooks to .git/hooks"
	mkdir -p .git/examplehooks
	cp -p githooks/* .git/hooks/

env-dev:
	@echo "Installing the dev environment."
	pip install -r requirements.txt

env-core:
	@echo "Installing sherpa packages for the core system (BeagleBone)."
	pip install -r requirements-core.txt

doc:
	@echo "Building documentation in doc-src and installing in doc."
	mkdir -p doc
	rm -fr doc/*
	cd doc-src && make singlehtml
	mv doc-src/build/singlehtml/* doc/

clean:
	./bin/rebuild_db
	rm -f ./conf/smap.sync
	./bin/configure

test: 
	@echo "Running tests."
	py.test

.PHONY: doc apidoc requirements env env-dev env-core clean githooks

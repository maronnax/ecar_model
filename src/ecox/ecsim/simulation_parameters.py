from collections import namedtuple

EcoxInitialConditions = namedtuple("EcoxInitialConditions", "pH, Si_init, As3_init, As5_init, P_init, D")

# Using this instead of named parameters so they can be mutable as records.  
class EcoxInitialConditions:
    def __init__(self, pH = 7.0, Si_init = 0.0, As3_init = 0.0, As5_init = 0.0, P_init = 0.0, D = 0.0):
        self.pH = pH
        self.Si_init = Si_init
        self.As3_init = As3_init
        self.As5_init = As5_init
        self.P_init = P_init
        self.D = D
        return


class EcoxConstants:
    def __init__(self, alpha = 0.858, beta = 0.25, gamma = 1.07, k_As3 = 6511, k_As5 = 376876, k_S = 872, k_P = 892853):
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        self.k_As3 = k_As3
        self.k_As5 = k_As5
        self.k_S = k_S
        self.k_P = k_P
        return 

class EcoxRunParameters:
    def __init__(self, t_dose_min = 0, t_mix_min = 0):
        self.t_dose_min = t_dose_min
        self.t_mix_min = t_mix_min
        
def checkInitialConditions():
    return

def makeStandardEcoxConstants():

    # Alpha Total number of available sites = alpha * [Fe(II)]tot
    # [Fe(IV)] = beta * [Fe(II)]ox unitless
    # Gamma = k1/k2, with k1 = rate of Fe(II) oxydation,
                     # and k2 = rate of As(III) oxydation

    # For the adsorption reaction :
    # [As(III)]dis + [S] -> [As(III)]ads, K_As3 = [As(III)]ads/[S]*[As(III)]dis
    # k_As3 = 6511  # L/mol

    # K_As5 = [As(V)]ads/[S]*[As(V)]dis
    #  k_As5 = 376876 # L/mol

    # K_Si = [Si]ads/[S]*[Si]dis
    # k_S = 872 # L/mol

    # K_P = [P]ads/[S]*[P]dis
    # k_P = 892853 # L/mol    

    return EcoxConstants(alpha = 0.858, \
                         beta = 0.25, \
                         gamma = 1.07, \
                         k_As3 = 6511, \
                         k_As5 = 376876, \
                         k_S = 872, \
                         k_P = 892853)

def validateEcoxConstants(ecox_constants):
    return

def validatedRunParamters(ecox_run_parameters):
    return

def validateInitialConditions(ecox_initial_conditions):
    return 

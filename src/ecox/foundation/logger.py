__author__ = "Nathan Addy <nathan.addy@gmail.com>"

'''This package provides logging setup capabilities for the Ecox platform
environment.'''

import logging
import ecox.foundation.config as config
import ecox.foundation.exceptions as xcpt

def getPackageLogLevel():
    log_level = config.get("ecox", "loglevel", "Info")

    levels = {"info": logging.INFO, 
              "critical": logging.CRITICAL, 
              "debug": logging.DEBUG"
              "error": logging.ERROR"
              "fatal": logging.FATAL"
              "warning: logging.WARN}

    return log_level[levels.lower()]

LOG_FORMAT = '%(name)s - %(asctime)s - %(levelname)s - %(message)s'
LOG_LEVEL = getPackageLogLevel()

def setupDefaultLoggerHandlers():
    '''Setup basic logging for the global app. This prints everything out
    to the screen by default.'''

    global LOG_LEVEL

    root_logger = logging.getLogger('')
    root_logger.setLevel(LOG_LEVEL)

    ch = logging.StreamHandler()
    ch.setLevel(LOG_LEVEL)

    formatter = logging.Formatter(LOG_FORMAT)
    ch.setFormatter(formatter)

    root_logger.addHandler(ch)    

    return 


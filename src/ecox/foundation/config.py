__author__ = "Nathan Addy <nathan.addy@gmail.com>"

'''Provides utilities for maintaining a configuration file for the
ecox system in the user's home directory.'''

import ecox.foundation.exceptions as xcpt
from ConfigParser import ConfigParser
import os

class ECOXConfiguration(object):

    '''Represents a configuration file that that holds values in the form
    section::key.

    The main class method is get, which allows for getting values with
    a default and also has build-in methods for automatically
    processing values found in the configuration.

    ''' 
    def __init__(self, fn):
        self.base_dir = os.path.abspath(os.path.split(fn)[0])
        self.fn = os.path.abspath(fn)
        if os.path.isfile(fn):
            self.contents = open(fn).read()
            self._conf = ConfigParser()
            self._conf.read(fn)
        else:
            self.contents = None
            self._conf = None
        return

    def getFilename(self):
        '''Get the original filename loaded in.'''
        return self.fn

    def initialized(self):
        '''Return True if data was read from the file'''
        return bool(self.contents)

    def get(self, sect, key, default = False, is_filename=False, type = False, time=False):
        '''Get a value for the value key in section sect.  
        
        If section::value not present, False is returned.
        is_filename = True will convert the value to an absolute fn defined relatively to the conf file
        type will convert the value to that type
        time = True will attempt to convert strings like 30, 30s, 30.0m, 10d, etc to a number of seconds.
        '''

        if self._conf == None: return default
        
        value = default if not self._conf.has_option(sect, key) else self._conf.get(sect,key)
        if is_filename and value != False:
            value = os.path.abspath(os.path.join(self.base_dir, value))

        if type: value = type(value)

        if time:
            value = ECOXConfiguration._getBestSecondsFromConfigString(value)

        return value
            
    def has(self, sect, key):
        '''Check if sect::key is present in the config object'''
        if self._conf == None: return False
        return self._conf.has_option(sect, key)


    @staticmethod
    def _getBestSecondsFromConfigString(conf_string):
        '''
        Takes a string like 20, 20.0, 21.3s 29m, 30.1h and returns a
        floating number of seconds.

        Keyword arguments:
        conf (str): A string representing a number using the [A<.B>[ shmd] syntax.

        Returns:
        float: the number of seconds represented by the string
        '''

        conf_string=conf_string.strip()

        S,M,H,D,T=1,2,3,4,5
        interval = S

        if conf_string[-1].lower() not in "smdhy": return float(conf_string)

        float_str, val = conf_string[:-1], conf_string[-1].lower()

        if val == "s":
            return float(float_str)
        elif val == "m":
            return float(float_str) * 60
        elif val == "h":
            return float(float_str) * 3600
        elif val == "d":
            return float(float_str) * 3600 * 24
        elif val == "y":
            return float(float_str) * 3600 * 24 * 365
        else:
            raise Exception("Unknown error")

        return 

def getDefaultConfigFileName():
    '''Get the filename for the default configuration file for a ecox
    file which is located in ~/.ecox.
    '''
    home_dir = os.path.expanduser("~")
    fn = os.path.join(home_dir, ".ecox")
    fn = os.path.abspath(fn)
    return fn

def getDefaultConfig():
    '''Return the parsed config object for the default system config file.'''
    return ECOXConfiguration(getDefaultConfigFileName())

def getDefaultConfigValue(section, value, default, **kwds):
    '''Pulls a section::value from the default configuration file
    (${root}/conf/ecox) and returns it.'''

    conf = getDefaultConfig()
    return conf.get(section, value, default, **kwds)


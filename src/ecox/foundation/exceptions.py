__author__ = "Nathan Addy <nathan.addy@gmail.com>"

'''Provides Exceptions for the Ecox foundational classes'''

class PureVirtualFunctionXcpt(Exception):
    '''Represents a pure virtual function that must be subclassed.'''
    def __init__(self):
        msg = "This function must be overridden by a subclass."
        super(PureVirtualFunctionXcpt, self).__init__(msg)
        return 

class NotImplementedXcpt(Exception):
    '''Debug type exception.  This fills a role of either "never get here"
    or "Implement me", depending on the context.  It should never show
    up at beta or beyond.
    '''
    def __init__(self):
        msg = ("This exception should never be thrown. It probably represents "
               "a logic error in the code.")
        super(NotImplementedXcpt, self).__init__(msg)
        return

class InputErrorXcpt(Exception):
    '''Represents a problem with the information specified by the user.'''
    pass


class CodingError(Exception): pass
class ModelError(Exception): pass

'''Module that countains the foundation, ecmodel, and ecsim modules'''

__author__ = "Nathan Addy <nathan.addy@gmail.com>"

def getVersion():
    '''Returns the version of ecox this code belongs to.'''
    import os

    module_directory = os.path.split(__file__)[0]
    version_file = os.path.join(module_directory, "VERSION")
    if not os.path.isfile(version_file):
        return "0"

    
    version = open(version_file).read().strip()
    return version

__version__ = getVersion()

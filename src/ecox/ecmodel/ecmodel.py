'''This module provides basic functions for simulating single
simulated runs of the EC Oxidation model.
'''

import numpy
from scipy.optimize import fsolve

# import simulation_parameters as params
# from simulation_parameters import *

import ecox.foundation
import  PyDSTool as dst
import random
from scipy.optimize import fsolve
import scipy.optimize as optimize

STIFF = True

def simulateECModel(params):
    '''this function simulates dosing, mixing, and instantaneous
    adsorption based on parameters passed in the argument. The output
    is a dictionary that contains a time series and molar
    concentrations for all species.
    '''

    d_rate   = (params['d_rate'] * 5.1822E-6) / 60 # converts coulombs / minute --> moles Fe / second
    t_dose   = params['t_dose'] # s 
    t_mix    = params['t_mix'] # s
    pH       = params['pH']
    Si_init  = params['Si_init']
    As3_init = params['As3_init']
    As5_init = params['As5_init']
    P_init   = params['P_init']
    O2_init  = params['O2_init']
    O2_sat   = params['O2_sat']
    k_app    = params['k_app'] * 10 ** (1.2 * (pH - 7.1)) # establishes the pH-dependence of k_app
    alpha    = params['alpha']
    beta     = params['beta']
    gamma    = params['gamma']
    k_As3    = params['k_As3']
    k_As5    = params['k_As5']
    k_S      = params['k_S']
    k_P      = params['k_P']
    
    if params['k_r'] == 'sat': 
        k_r = 0
        O2_condition = 'sat' # sets the permanent O2 saturation condition
    else:                    
        k_r = params['k_r'] / 3600     # converts hours^-1 --> seconds^-1
        O2_condition = 'dyn' # sets the dynamic O2 condition

    if 'k' in params['pH_deps']: # uses table to find k_As5, k_P based on pH, if adsorption is pH-dependent
        if pH == 6.0:
            k_As5 = 10**6.1
            k_P = 10**6.4
        elif pH == 6.6:
            k_As5 = 10**6.0
            k_P = 10**6.2
        elif pH == 7.1:
            k_As5 = 10**5.76
            k_P = 10**6.06
        elif pH == 7.2:
            k_As5 = 10**5.6
            k_P = 10**5.9
        elif pH == 7.5:
            k_As5 = 10**5.4
            k_P = 10**5.6
        elif pH == 7.8:
            k_As5 = 10**5.2
            k_P = 10**5.3
        elif pH == 8.1:
            k_As5 = 10**4.9
            k_P = 10**5.1
        elif pH == 8.6:
            k_As5 = 10**4.4
            k_P = 10**4.6

        elif pH < 6.0: # these are approximations based on the pH table
            k_As5 = 10**6.1
            k_P = 10**6.4
        elif pH > 6.0 and pH < 6.6:
            k_As5 = 10**6.0
            k_P = 10**6.2
        elif pH > 6.6 and pH < 7.1:
            k_As5 = 10**5.76
            k_P = 10**6.06
        elif pH > 7.1 and pH < 7.2:
            k_As5 = 10**5.6
            k_P = 10**5.9
        elif pH > 7.2 and pH < 7.5:
            k_As5 = 10**5.4
            k_P = 10**5.6
        elif pH > 7.5 and pH < 7.8:
            k_As5 = 10**5.2
            k_P = 10**5.3
        elif pH > 7.8 and pH < 8.1:
            k_As5 = 10**4.9
            k_P = 10**5.1
        elif pH > 8.1:
            k_As5 = 10**4.4
            k_P = 10**4.6

    if 'g' in params['pH_deps']: # uses correlation to find gamma, if gamma is pH-dependent
        gamma = 10**(1.125 * pH - 8.0748)

    # This dictionary contains the modified parameters, and can be passed to other functions:

    reduced_params = {"d_rate": d_rate,
                      "t_dose": t_dose,
                      "t_mix": t_mix,
                      "pH": pH,
                      "Si_init": Si_init,
                      "As3_init": As3_init,
                      "As5_init": As5_init,
                      "P_init": P_init,
                      "O2_init": O2_init,
                      "O2_sat": O2_sat,
                      "k_app": k_app,
                      "k_r": k_r,
                      "alpha": alpha,
                      "beta": beta,
                      "gamma": gamma,
                      "k_As3": k_As3,
                      "k_As5": k_As5,
                      "k_S": k_S,
                      "k_P": k_P}
    
    # The following procedure is used to find steady-state concentrations after dosing and mixing:
    # Solve diffeq from t = 0 -> t_dose using equations
    # Set D = 0
    # Solve diffeq from t = t_dose -> t_mix using equations
    # Solve for steady state values using adsorption constraints applied to output of simulation

    DSargs  = dst.args ( name = 'EC Oxidation System with Fe2 Dosing' ) # solves equations under dosing

    DSargs.pars = {"D": d_rate, \
                   "kapp": k_app, \
                   "beta": beta, \
                   "O2_sat": O2_sat, \
                   "k_r": k_r, \
                   "gammaP": gamma } # 'gamma' is an
                                      # illegal token in varspecs.

    DSargs.ics = { "Fe2_dis": 0.0, "Fe2_ox": 0.0, \
                   "As3_tot": As3_init, "As3_ox": 0.0, \
                   "As5_tot": As5_init, \
                   "O2": O2_init}

    if O2_condition == 'sat': # defines kinetics under permanent O2 saturation condition
        DSargs.varspecs = {"Fe2_dis": "D - kapp * O2 * Fe2_dis",
                            "Fe2_ox": "kapp * O2 * Fe2_dis",
                            "As3_tot": "-beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                            "As3_ox": "beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                            "As5_tot": "beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                            #"O2": "k_r * (O2_sat - O2) - kapp * O2 * Fe2_dis"}
                            "O2": "0"}
        
    else:                     # defines kinetics under dynamic O2 condition
        DSargs.varspecs = {"Fe2_dis": "D - kapp * O2 * Fe2_dis",
                            "Fe2_ox": "kapp * O2 * Fe2_dis",
                            "As3_tot": "-beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                            "As3_ox": "beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                            "As5_tot": "beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                            "O2": "k_r * (O2_sat - O2) - kapp * O2 * Fe2_dis"}
                            #"O2": "0"}
        
    DSargs.xdomain = {"Fe2_dis": [0, 1000]}
    DSargs.tdomain =  [0 , t_dose]
    DSargs.algparams = {'init_step':0.1, 'atol': 1e-8,'strictdt': True, 'stiff': True}

    ODE   = dst.Generator.Vode_ODEsystem( DSargs )

    traj_dose = ODE.compute ( 'EC Oxidation with Dosing' )
    pts_dose   = traj_dose.sample ( dt = 60)
    
    DSargs  = dst.args ( name = 'EC Oxidation system post Fe2 dose' ) # solves equations under mixing

    DSargs.pars = {"kapp": k_app, \
                   "beta": beta, \
                   "O2_sat": O2_sat, \
                   "k_r": k_r, \
                   "gammaP": gamma }

    Fe2_dis_mix0 = pts_dose["Fe2_dis"][-1]
    Fe2_ox_mix0 = pts_dose["Fe2_ox"][-1]
    As3_tot_mix0 = pts_dose["As3_tot"][-1]
    As3_ox_mix0 = pts_dose["As3_ox"][-1]
    As5_tot_mix0 = pts_dose["As5_tot"][-1]
    O2_mix0 = pts_dose["O2"][-1]


    DSargs.ics = { "Fe2_dis": Fe2_dis_mix0, "Fe2_ox": Fe2_ox_mix0, \
                   "As3_tot": As3_tot_mix0, "As3_ox": As3_ox_mix0, \
                   "As5_tot": As5_tot_mix0, \
                   "O2": O2_mix0}

    if O2_condition == 'sat': # defines kinetics under permanent O2 saturation condition
        DSargs.varspecs = {"Fe2_dis": "-kapp * O2 * Fe2_dis",
                        "Fe2_ox": "kapp * O2 * Fe2_dis",
                        "As3_tot": "-beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                        "As3_ox": "beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                        "As5_tot": "beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                        #"O2": "k_r * (O2_sat - O2) - kapp * O2 * Fe2_dis"}
                        "O2": "0"}
        
    else:                     # defines kinetics under dynamic O2 condition
        DSargs.varspecs = {"Fe2_dis": "-kapp * O2 * Fe2_dis",
                        "Fe2_ox": "kapp * O2 * Fe2_dis",
                        "As3_tot": "-beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                        "As3_ox": "beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                        "As5_tot": "beta * kapp * O2 * Fe2_dis / (1 + (gammaP * Fe2_dis) / As3_tot)",
                        "O2": "k_r * (O2_sat - O2) - kapp * O2 * Fe2_dis"}
                        #"O2": "0"}

    DSargs.xdomain = {"Fe2_dis": [0, 1000]}
    DSargs.tdomain =  [0 , t_mix]
    DSargs.algparams = {'init_step':0.1, 'atol': 1e-8, 'strictdt': True, 'stiff': True}

    ODE = dst.Generator.Vode_ODEsystem( DSargs )

    traj_mix = ODE.compute ('EC Oxidation system post Fe2 Dosing')
    pts_mix = traj_mix.sample ( dt = 60 )

    all_model_variables = calculateModelVariables(pts_dose, pts_mix, reduced_params) # steady state values computed from output

    #Output Keys:
    # t: time series (list)
    # Fe2_dis: time series for Fe2_dis (list)
    # As3_dis: time series for As3_dis (list)
    # As3_ads: time series for As3_ads (list)
    # As5_dis: time series for As5_dis (list)
    # As5_ads: time series for As5_ads (list)
    # O2: time series for O2 (list)
    # Fe2_dis_final: final Fe2_dis
    # Si_dis_final: final Si_dis
    # Si_ads_final: final Si_ads
    # P_dis_final: final P_dis
    # P_ads_final: final P_ads
    # As3_dis_final: final As3_dis
    # As3_ads_final: final As3_ads
    # As5_dis_final: final As5_dis
    # As5_ads_final: final As5_ads
    # O2_final: final O2
    
    return all_model_variables

# both of these are needed for the CalculateModelVariables function
def checkSolutionIsPositive(vect):
    return False not in [x >= 0 for x in vect]

def __generateRandomVector( vector ):
    return [ random.uniform(0,x) for x in vector]

def calculateModelVariables(pts_dose, pts_mix, global_vars):
    model_variables = {}

    # Loop over the simulated variables, make sure they match up, and
    # put them together. Finish by making the time variable, which is
    # not present in keys().
    for key in pts_dose.keys():
        # Make sure that boundary values match up.
        assert(pts_dose[key][-1] == pts_mix[key][0])
        model_variables[key] = numpy.hstack((pts_dose[key], pts_mix[key][1:]))
        model_variables[key] = numpy.array( model_variables[key], copy = False)
    else:
        t_dose = pts_dose["t"]
        t_mix = pts_mix["t"]
        t_mix = t_mix + t_dose[-1] # Reset add the time to the first one
        model_variables["t"] = numpy.hstack((t_dose, t_mix[1:]))

        non_t_cols = filter(lambda x: x != "t", model_variables.keys())

        for x in non_t_cols:
            try:
                assert(len(model_variables["t"]) == \
                       len(model_variables[x]))
            except AssertionError:
                err = ("Error in calculateModelVariables function, len(\"t\") does" \
                       "not equal that of the column '%s'" % x)
                raise exceptions.CodingError(err)

    # Get final values of these variables
    Fe2_ox = model_variables["Fe2_ox"][-1]
    Fe2_dis = model_variables["Fe2_dis"][-1]
    As3_tot = model_variables["As3_tot"][-1]
    As5_tot = model_variables["As5_tot"][-1]
    O2 = model_variables["O2"][-1]
    Si_tot = global_vars["Si_init"]  # These don't change through simulation
    P_tot = global_vars["P_init"]

    # This introduces the adsorption constraint equations (see makeVectorFunctionClosure)
    constraintFunction = makeVectorFunctionClosure( Fe2_ox, As3_tot, As5_tot, Si_tot, P_tot, \
                                                        global_vars["alpha"], \
                                                        global_vars["k_As3"], global_vars["k_As5"], \
                                                        global_vars["k_S"], global_vars["k_P"])

    # This uses iteration to make sure output satisfies adsorption equations within a tolerance
    tries = 0
    max_tries = 10000
    have_sol = False
    while not have_sol:
        if tries >= max_tries:
            raise Exception("Could not find a solution in an acceptable time. Maximum iterations exceeded.")

        initial_cond_alt = __generateRandomVector( [Si_tot, P_tot, As3_tot, As5_tot] )
        solution_vect_alt = optimize.fsolve(constraintFunction, initial_cond_alt, xtol = 1e-9)
        Si_dis, P_dis, As3_dis, As5_dis = solution_vect_alt
        Si_ads = Si_tot - Si_dis
        P_ads = P_tot - P_dis
        As3_ads = As3_tot - As3_dis
        As5_ads = As5_tot - As5_dis

        # This makes sure everything is positive
        have_sol = checkSolutionIsPositive([Si_dis, P_dis, As3_dis, As5_dis, Si_ads, P_ads, As3_ads, As5_ads])
        tries += 1
    # This defines the adsorbed quantities by subtracting dissolved from initial
    Si_ads = Si_tot - Si_dis
    P_ads = P_tot - P_dis
    As3_ads = As3_tot - As3_dis
    As5_ads = As5_tot - As5_dis

    model_variables["Si_ads_final"] = Si_ads
    model_variables["Si_dis_final"] = Si_dis
    model_variables["P_ads_final"] = P_ads
    model_variables["P_dis_final"] = P_dis
    model_variables["As3_ads_final"] = As3_ads
    model_variables["As3_dis_final"] = As3_dis
    model_variables["As5_ads_final"] = As5_ads
    model_variables["As5_dis_final"] = As5_dis
    model_variables["Fe2_dis_final"] = Fe2_dis
    model_variables["O2_final"] = O2

    return model_variables

def makeVectorFunctionClosure(Fe2_ox, As3_tot, As5_tot, Si_tot, P_tot, alpha, k_As3, k_As5, k_S, k_P):
    # This function takes the parameters, as well as the quantity of
    # Fe2_ox, and returns a function that describes the steady state

    # This is called a closure, which plugs in the parameter values
    # into a function and returns it, which can then be minimized, and
    # generally used like any other function.

    def vectorFunc(vect):
        # This is a vector function that evaluates the eight
        # functions that define the constraints by minimizing these 8 variables.

        # This is how the parameters should pack into vect.
        S_dis, P_dis, As3_dis, As5_dis = vect

        S_ads = Si_tot - S_dis
        P_ads = P_tot - P_dis
        As3_ads = As3_tot - As3_dis
        As5_ads = As5_tot - As5_dis

        # This is the common denominator for all the steady state constraints.
        denominator = 1 + (k_S * S_dis) + (k_P * P_dis) + (k_As3 * As3_dis) + (k_As5 * As5_dis)

        # Problems
        # The four steady-state constraints
        c5 = alpha * Fe2_ox * k_S * S_dis / denominator - S_ads
        c6 = alpha * Fe2_ox * k_P * P_dis / denominator - P_ads
        c7 = alpha * Fe2_ox * k_As3 * As3_dis / denominator - As3_ads
        c8 = alpha * Fe2_ox * k_As5 * As5_dis / denominator - As5_ads

        # Package them up into one vector and return it.
        return [c5, c6, c7 , c8]
    return vectorFunc

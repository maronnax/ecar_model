import logging

MODULE_LOGGER_NAME = 'ECOX'
BASIC_MODULE_LOGGING_LEVEL = logging.ERROR

# Intitialize the module logger.
def initializeLogger():
    logger = logging.getLogger(MODULE_LOGGER_NAME)
    logger.setLevel(BASIC_MODULE_LOGGING_LEVEL)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    formatter.datefmt = '%m-%d-%y %H:%M:%S'
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return

def getLogger():
    return logging.getLogger(MODULE_LOGGER_NAME)

initializeLogger()

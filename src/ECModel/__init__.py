from ecmodel import *
import pdb

from simulation_parameters import EcoxInitialConditions, EcoxConstants, EcoxRunParameters
from simulation_parameters import makeStandardEcoxConstants
from logger import *



def getVersion():
    import os

    module_directory = os.path.split(__file__)[0]
    version_file = os.path.join(module_directory, "VERSION")
    if not os.path.isfile(version_file):
        return "0"

    
    version = open(version_file).read().strip()
    return version

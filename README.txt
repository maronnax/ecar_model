Overview
=========
This code represents a model of the EC Oxidation process that can be
simulated.  It also supports bifurcation analysis, and optimization of
model parameters.


Physical Description
====================
The physical system being modeled is an electrochemical process in
water. The system has two stages, a dosing period in which iron is
added and a mixing period, in which there is no dosing. Adsorption
processes are only considered at the end of mixing, and steady state
is achieved instantaneously.


Installation Guide
==================
1. Install SourceTree from this link: http://sourcetreeapp.com/

2. Email Nathan Addy to get access to the ecar model via BitBucket: nathan.addy@gmail.com 

3. Install Python 2.7.6 from this link: https://www.python.org/download/releases/2.7.6/

4. Install SciPy 0.13.2 from this link: http://sourceforge.net/projects/scipy/files/scipy/0.13.2/

5. Install NumPy 1.8.0 from this link: http://sourceforge.net/projects/numpy/files/NumPy/1.8.0/

6. Install MatPlotLib 1.3.1 from this link: http://matplotlib.org/downloads.html

7. Update your system path so Python can find PyDSTool (located in ecar_model/src):

	For Windows, use these instructions: http://www.ni.gsu.edu/~rclewley/PyDSTool/GettingStarted.html#head-1f38ca5c60b2f42844a6a6424d6e1d1a76d1bcd8
	
	For Linux/OSX, use these instructions: http://www.ni.gsu.edu/~rclewley/PyDSTool/GettingStarted.html#head-fac1fbe3d8aaad68689ce71a932ad61343abd87d

8. Verify that you have the correct Python module versions by entering the following into the Python command line:
	
	import libname 
	print libname.__version__

	('libname' is a placeholder for scipy, numpy, matplotlib, or PyDSTool)

9. Navigate to ecar_model/scripts/users and create a directory with your first name (this is where your output files will be saved).

Further Reading
===============
Look through the Getting Started guide (located in the same directory).